const cardData = [{"id":1,"card_number":"5602221055053843723","card_type":"china-unionpay","issue_date":"5/25/2021","salt":"x6ZHoS0t9vIU","phone":"339-555-5239"},
{"id":2,"card_number":"3547469136425635","card_type":"jcb","issue_date":"12/18/2021","salt":"FVOUIk","phone":"847-313-1289"},
{"id":3,"card_number":"5610480363247475108","card_type":"china-unionpay","issue_date":"5/7/2021","salt":"jBQThr","phone":"348-326-7873"},
{"id":4,"card_number":"374283660946674","card_type":"americanexpress","issue_date":"1/13/2021","salt":"n25JXsxzYr","phone":"599-331-8099"},
{"id":5,"card_number":"67090853951061268","card_type":"laser","issue_date":"3/18/2021","salt":"Yy5rjSJw","phone":"850-191-9906"},
{"id":6,"card_number":"560221984712769463","card_type":"china-unionpay","issue_date":"6/29/2021","salt":"VyyrJbUhV60","phone":"683-417-5044"},
{"id":7,"card_number":"3589433562357794","card_type":"jcb","issue_date":"11/16/2021","salt":"9M3zon","phone":"634-798-7829"},
{"id":8,"card_number":"5602255897698404","card_type":"china-unionpay","issue_date":"1/1/2021","salt":"YIMQMW","phone":"228-796-2347"},
{"id":9,"card_number":"3534352248361143","card_type":"jcb","issue_date":"4/28/2021","salt":"zj8NhPuUe4I","phone":"228-796-2347"},
{"id":10,"card_number":"4026933464803521","card_type":"visa-electron","issue_date":"10/1/2021","salt":"cAsGiHMFTPU","phone":"372-887-5974"}] 

// Find all cards that were issued before June.

let cardsIssuedBeforeJune = cardData.reduce((accumulator, current) => {
    let date = current.issue_date.split('/');
    if (date[0] < 6) {
        accumulator.push(current);
    }
    return accumulator;
}, []);

console.log(cardsIssuedBeforeJune);

// assign cvv to each card

let addedCardCVV = cardData.map((card) => {
    card.CVV = Math.floor(Math.random()*(999-100+1)+100);
    return card;
});

console.log(addedCardCVV);

// new field to each card to indicate if the card is valid or not

let addedCardValidation = cardData.map((card) => {
    card.validation = true;
    return card;
});

console.log(addedCardValidation);

// Invalidate all cards issued before March

let validatedCards = addedCardValidation.reduce((accumulator, current) => {
    let date = current.issue_date.split('/');
    if (date[0] < 3) {
        current.validation = false;
    }
    accumulator.push(current);
    return accumulator;
}, []);

console.log(validatedCards);

// Sort the data into ascending order of issue date

let sortedCards = cardData.sort((first, second) => {
    let issueDate1 = first.issue_date.split('/');
    let issueDate2 = second.issue_date.split('/');
    if (parseInt(issueDate1[0]) > parseInt(issueDate2[0])) {
        return 1;
    } else if (parseInt(issueDate1[0]) < parseInt(issueDate2[0])) {
        return -1;
    } else {
        if (parseInt(issueDate1[1]) > parseInt(issueDate2[1])) {
            return 1;
        } else {
            return -1;
        }
    }
});

console.log(sortedCards);

// Group the data according to which cards were assigned in which month

let cardsGroupedByMonth = cardData.reduce((accumulator, current) => {
    let cardMonth = (current.issue_date.split('/')[0]);
    if (accumulator[cardMonth]) {
        accumulator[cardMonth] = {...accumulator[cardMonth], current};
    } else {
        accumulator[cardMonth] = current;
    }
    return accumulator;
}, {});

console.log(cardsGroupedByMonth);

